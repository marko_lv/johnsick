/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package johnsick.logic;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPTable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static johnsick.logic.ReportConstants.*;
import johnsick.model.Fakultatyv;
import johnsick.model.Student;
import johnsick.model.Visit;
import johnsick.model.dao.FakultatyvDAO;
import johnsick.model.dao.StudentDAO;
import johnsick.model.dao.VisitDAO;

/**
 *
 * @author mk
 */
public class ReportHelper {

    public static void getFakVisits(Long fakultatyvId) {

        Fakultatyv fak = new FakultatyvDAO().get(fakultatyvId);
        if (fak != null) {
            ReportConstants rc = new ReportConstants();
            Document document = rc.GetDocument("звіт");
            try {
                document.add(new Paragraph("Звіт", NORMAL));
                document.add(new Paragraph("Факультатив №" + fak.getId() + "; " + fak.getName(), NORMAL));
                String invoiceDate = "";
                java.util.Date utilDate = new Date();
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

                invoiceDate = dateFormat.format(new java.sql.Timestamp(utilDate.getTime()));

                document.add(new Paragraph("від " + invoiceDate, NORMAL));

                for (java.sql.Date date : new VisitDAO().getAllVisitIds(fakultatyvId)) {
                    Integer i = 1;
                    Visit visit = new VisitDAO().getFirst(date);
                    PdfPTable table = new PdfPTable(4);
                    table.setTotalWidth(new float[]{100, 80, 80, 80}); //фіксована ширина колонок
                    table.setLockedWidth(true); //блокуємо ширину
                    table.setHorizontalAlignment(table.ALIGN_LEFT); //вирівнювання таблиці
                    table.setSpacingBefore(15); //додати проміжок перед  таблицею

                    table.setHeaderRows(1);//всього три рядки в хідері
                    table.addCell(new Paragraph("дата", NORMAL_9));
                    table.addCell(new Paragraph("лекційні", NORMAL_9));
                    table.addCell(new Paragraph("практичні", NORMAL_9));
                    table.addCell(new Paragraph("лабораторні", NORMAL_9));

                    table.addCell(new Paragraph(date.toString(), NORMAL_9));

                    table.addCell(new Paragraph(visit.getLectionHoursCount().toString(), NORMAL_9));
                    table.addCell(new Paragraph(visit.getPracticHoursCount().toString(), NORMAL_9));
                    table.addCell(new Paragraph(visit.getLaborHoursCount().toString(), NORMAL_9));

                    document.add(table);

                    document.add(new Paragraph("Студенти", NORMAL));
                    table = new PdfPTable(5);
                    table.setTotalWidth(new float[]{200, 50, 50, 50, 50}); //фіксована ширина колонок
                    table.setLockedWidth(true); //блокуємо ширину
                    table.setHorizontalAlignment(table.ALIGN_LEFT); //вирівнювання таблиці
                    table.setSpacingAfter(25); //додати проміжок перед  таблицею
                    table.setSpacingBefore(15); //додати проміжок перед  таблицею

                    table.setHeaderRows(1);//всього три рядки в хідері
                    table.addCell(new Paragraph("ПІБ", NORMAL_9));
                    table.addCell(new Paragraph("при-сутність", NORMAL_9));
                    table.addCell(new Paragraph("оцінка за лекційні", NORMAL_9));
                    table.addCell(new Paragraph("оцінка за практичні", NORMAL_9));
                    table.addCell(new Paragraph("оцінка за лабораторні", NORMAL_9));

                    List<Student> students = new StudentDAO().getAll();
                    for (Visit vis : new VisitDAO().getAll(date)) {
                        table.addCell(new Paragraph(
                                students
                                        .stream()
                                        .filter(stud -> stud.getId().equals(vis.getStudentId()))
                                        .findFirst()
                                        .get()
                                        .getFullName(),
                                NORMAL_9));
                        table.addCell(new Paragraph(vis.getVisited()? " \u221A": "", NORMAL_9));
                        table.addCell(new Paragraph(vis.getLectionMark().toString(), NORMAL_9));
                        table.addCell(new Paragraph(vis.getPracticMark().toString(), NORMAL_9));
                        table.addCell(new Paragraph(vis.getLaborMark().toString(), NORMAL_9));
                    }
                    document.add(table);

                }
            } catch (DocumentException ex) {
                Logger.getLogger(ReportHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
            rc.CloseDocument(document);

            rc.ShowTempFile();

        }
    }

}
