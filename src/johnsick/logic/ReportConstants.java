package johnsick.logic;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Desktop;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.print.DocFlavor;
import javax.print.DocPrintJob;
import javax.print.PrintException;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.SimpleDoc;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.JobName;
import javax.print.attribute.standard.MediaSizeName;

/**
 *
 * @author mk
 */
public class ReportConstants {

    public static final Font NORMAL = FontFactory.getFont("resources/fonts/UbuntuMono-R.ttf", BaseFont.IDENTITY_H, true, 10f);
    public static final Font NORMAL_UNDERLINE = FontFactory.getFont("resources/fonts/UbuntuMono-R.ttf", BaseFont.IDENTITY_H, true, 10f, Font.NORMAL | Font.UNDERLINE);
    public static final Font NORMAL_9 = FontFactory.getFont("resources/fonts/UbuntuMono-R.ttf", BaseFont.IDENTITY_H, true, 9f);
    public static final Font NORMAL_8 = FontFactory.getFont("resources/fonts/UbuntuMono-R.ttf", BaseFont.IDENTITY_H, true, 8f);
    public static final Font NORMAL_7 = FontFactory.getFont("resources/fonts/UbuntuMono-R.ttf", BaseFont.IDENTITY_H, true, 7f);
    public static final Font NORMAL_5 = FontFactory.getFont("resources/fonts/UbuntuMono-R.ttf", BaseFont.IDENTITY_H, true, 6f);
    public static final Font NORMAL_8_ITALIC = FontFactory.getFont("resources/fonts/UbuntuMono-R.ttf", BaseFont.IDENTITY_H, true, 8f, Font.ITALIC);
    public static final Font NORMAL_7_ITALIC = FontFactory.getFont("resources/fonts/UbuntuMono-R.ttf", BaseFont.IDENTITY_H, true, 7f, Font.ITALIC);
    public static final Font BOLD_9 = FontFactory.getFont("resources/fonts/UbuntuMono-B.ttf", BaseFont.IDENTITY_H, true, 9f);
    public static final Font NORMAL_BOLD = FontFactory.getFont("resources/fonts/UbuntuMono-B.ttf", BaseFont.IDENTITY_H, true, 10f, Font.BOLD);
    public static final Font BOLD_11 = FontFactory.getFont("resources/fonts/UbuntuMono-B.ttf", BaseFont.IDENTITY_H, true, 11f, Font.BOLD);
    public static File tempFile;
    public static final Rectangle PAGESIZE_A4_PORTRAIT = new Rectangle(PageSize.A4);
    public static final Rectangle PAGESIZE_A4_LANDSCAPE = new Rectangle(PageSize.A4.rotate());

    public Document GetDocument(String docName) {
        return GetDocument(docName, PAGESIZE_A4_PORTRAIT);
    }

    public Document GetDocument(String docName, Rectangle rect) {
        try {

            String fileName = "дзонсік-"
                    + docName
                    + "-"
                    + Helper.getCurrentDateAsString();
            
                tempFile = File.createTempFile(fileName, ".pdf");
            
            // 216f, 720f
//            Rectangle pagesize = PAGESIZE_A4;
            //ліве, праве, верх, низ 
            Document document = new Document(rect, 36f, 36f, 10f, 10f);//margins are added
            try {
                PdfWriter.getInstance(document, new FileOutputStream(tempFile));
            } catch (FileNotFoundException | DocumentException ex) {
                Logger.getLogger(ReportConstants.class.getName()).log(Level.SEVERE, null, ex);
            }
            document.open();
            return document;
        } catch (IOException ex) {
            Logger.getLogger(ReportConstants.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void CloseDocument(Document document) {
        document.close();
    }

    public void ShowTempFile() {
        try {
            Desktop.getDesktop().open(tempFile);
            tempFile.deleteOnExit();
        } catch (IOException ex) {
            Logger.getLogger(ReportConstants.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void PrintTempFile() {
        Desktop desktop = Desktop.getDesktop();
        if (desktop.isSupported(Desktop.Action.PRINT)) {
            try {
                Desktop.getDesktop().print(tempFile);
                tempFile.deleteOnExit();
            } catch (IOException ex) {
                Logger.getLogger(ReportConstants.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            DocFlavor flavor = DocFlavor.INPUT_STREAM.AUTOSENSE;
            PrintRequestAttributeSet requestAttributeSet = new HashPrintRequestAttributeSet();
            requestAttributeSet.add(MediaSizeName.ISO_A4);
            requestAttributeSet.add(new JobName(tempFile.getName(), Locale.ENGLISH));
            PrintService[] services = PrintServiceLookup.lookupPrintServices(flavor, requestAttributeSet);
            PrintService defaultService = PrintServiceLookup.lookupDefaultPrintService();
            //PrintService service = ServiceUI.printDialog(null, 100, 100, services, defaultService, flavor, requestAttributeSet);
            if (defaultService != null) {
                DocPrintJob job = defaultService.createPrintJob();
                SimpleDoc doc = null;
                try {
                    doc = new SimpleDoc(new FileInputStream(tempFile), flavor, null);
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(ReportConstants.class.getName()).log(Level.SEVERE, null, ex);
                }
                try {
                    job.print(doc, requestAttributeSet);
                    tempFile.deleteOnExit();
                } catch (PrintException ex) {
                    Logger.getLogger(ReportConstants.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

   
}
