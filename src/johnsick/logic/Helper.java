/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package johnsick.logic;

import java.awt.Component;
import java.awt.EventQueue;
import java.awt.event.MouseEvent;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.EventObject;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.text.JTextComponent;
import johnsick.model.Student;
import johnsick.model.Visit;
import johnsick.model.dao.StudentDAO;
import johnsick.model.dao.VisitDAO;

/**
 *
 * @author JohnSick
 */
public class Helper {

    public static JTable getFakResultTableModel(Long fakulatyvId) {

        final DefaultTableModel model = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column == 2
                        || column == 3
                        || column == 4
                        || column == 5;
            }
        };
        JTable result = new JTable(model) {
            @Override // Always selectAll()
            public boolean editCellAt(int row, int column, EventObject e) {
                boolean result = super.editCellAt(row, column, e);
                final Component editor = getEditorComponent();
                if (editor == null || !(editor instanceof JTextComponent)) {
                    return result;
                }
                if (e instanceof MouseEvent) {
                    EventQueue.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            ((JTextComponent) editor).selectAll();
                        }
                    });

                } else {
                    ((JTextComponent) editor).selectAll();
                }
                return result;
            }

            @Override
            public Class getColumnClass(int column) {
                if (column == 0) {
                    return Long.class;
                }
                if (column == 2) {
                    return Boolean.class;
                }
                return String.class;
            }
        };

        model.addColumn("");
        model.addColumn("Студент");
        model.addColumn("Присутність");
        model.addColumn("Оцінка Лекція");
        model.addColumn("Оцінка Практична");
        model.addColumn("Оцінка Лабораторна");

        result.getColumnModel().getColumn(0).setMaxWidth(0);
        result.getColumnModel().getColumn(0).setMinWidth(0);
        result.getColumnModel().getColumn(0).setWidth(0);
        result.getColumnModel().getColumn(0).setPreferredWidth(0);
        result.getColumnModel().getColumn(1).setPreferredWidth(240);
        result.getColumnModel().getColumn(2).setPreferredWidth(100);
        result.getColumnModel().getColumn(3).setPreferredWidth(100);
        result.getColumnModel().getColumn(4).setPreferredWidth(100);
        result.getColumnModel().getColumn(5).setPreferredWidth(100);

        new StudentDAO().getAllByFakultatyv(fakulatyvId).forEach((stud) -> {
            model.insertRow(model.getRowCount(),
                    new Object[]{
                        stud.getId(),
                        stud.getFullName(),
                        Boolean.TRUE,
                        "", "", ""
                    });
        });
        return result;
    }

    public static void saveVisitFak(TableModel model, Long fakId, Date date,
            Double lectionHours, Double practicHours, Double laborHours) {

        for (int i = 0; i < model.getRowCount(); i++) {
            Long studentId = Long.parseLong(model.getValueAt(i, 0).toString());
            Boolean visited = Boolean.parseBoolean(model.getValueAt(i, 2).toString());
            Double lectionMark = Double.parseDouble(model.getValueAt(i, 3).toString().isEmpty() ? "0" : model.getValueAt(i, 3).toString());
            Double practicMark = Double.parseDouble(model.getValueAt(i, 4).toString().isEmpty() ? "0" : model.getValueAt(i, 4).toString());
            Double laborMark = Double.parseDouble(model.getValueAt(i, 5).toString().isEmpty() ? "0" : model.getValueAt(i, 5).toString());
            Visit visit = new Visit(fakId, studentId, date, visited, lectionMark, practicMark,
                    laborMark, lectionHours, practicHours, laborHours);
            new VisitDAO().save(visit);
        }

    }
    
    public static String getCurrentDateAsString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss-SSS");
        String string = dateFormat.format(new java.util.Date());
        return string;
    }
}
