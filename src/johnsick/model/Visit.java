/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package johnsick.model;

import java.sql.Date;

/**
 *
 * @author dzonsyck
 */
public class Visit {

    private Long fakulatyvId;
    private Long studentId;
    private Date visitDate;
    private Boolean visited;
    private Double lectionMark;
    private Double practicMark;
    private Double laborMark;
    private Double lectionHoursCount;
    private Double practicHoursCount;
    private Double laborHoursCount;

    public Visit(Long fakulatyvId, 
            Long studentId, 
            Date visitDate, 
            Boolean visited, 
            Double lectionMark, 
            Double practicMark, 
            Double laborMark, 
            Double lectionHoursCount, 
            Double practicHoursCount, 
            Double laborHoursCount) {
        this.fakulatyvId = fakulatyvId;
        this.studentId = studentId;
        this.visitDate = visitDate;
        this.visited = visited;
        this.lectionMark = lectionMark;
        this.practicMark = practicMark;
        this.laborMark = laborMark;
        this.lectionHoursCount = lectionHoursCount;
        this.practicHoursCount = practicHoursCount;
        this.laborHoursCount = laborHoursCount;
    }

    public Long getFakulatyvId() {
        return fakulatyvId;
    }

    public void setFakulatyvId(Long fakulatyvId) {
        this.fakulatyvId = fakulatyvId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public Boolean getVisited() {
        return visited;
    }

    public void setVisited(Boolean visited) {
        this.visited = visited;
    }

    public Double getLectionMark() {
        return lectionMark;
    }

    public void setLectionMark(Double lectionMark) {
        this.lectionMark = lectionMark;
    }

    public Double getPracticMark() {
        return practicMark;
    }

    public void setPracticMark(Double practicMark) {
        this.practicMark = practicMark;
    }

    public Double getLaborMark() {
        return laborMark;
    }

    public void setLaborMark(Double laborMark) {
        this.laborMark = laborMark;
    }

    public Double getLectionHoursCount() {
        return lectionHoursCount;
    }

    public void setLectionHoursCount(Double lectionHoursCount) {
        this.lectionHoursCount = lectionHoursCount;
    }

    public Double getPracticHoursCount() {
        return practicHoursCount;
    }

    public void setPracticHoursCount(Double practicHoursCount) {
        this.practicHoursCount = practicHoursCount;
    }

    public Double getLaborHoursCount() {
        return laborHoursCount;
    }

    public void setLaborHoursCount(Double laborHoursCount) {
        this.laborHoursCount = laborHoursCount;
    }

}
