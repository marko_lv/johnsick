package johnsick.model.db;

import java.awt.HeadlessException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author JohnSick
 */
public class ConnectionFactory {

    private static ConnectionFactory instance = new ConnectionFactory();

    public static Connection connection = null;
    public static JohnSickConnection currentConnection = null;

    public static class JohnSickConnection {

        public String DbUrl = "";
        public String DbUser = "";
        public String DbPassword = "";
        public String DbName = "";

        @Override
        public String toString() {
            return this.DbName;
        }
    }

    //private constructor
    private ConnectionFactory() {
        /*try {
            Class.forName(DRIVER_CLASS);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }*/
    }

    private Connection createConnection() {
        connection = null;
        try {
            Class.forName("org.sqlite.JDBC");
            //Connection conn = DriverManager.getConnection("jdbc:sqlite:D:/mk/JohnSick/johnsick.db");
            connection = DriverManager.getConnection("jdbc:sqlite:" + currentConnection.DbName);
        } catch (ClassNotFoundException | SQLException | HeadlessException e) {
            JOptionPane.showMessageDialog(null,
                    "Не можу під'єднатися до бази. " + currentConnection.DbName 
                    + "\n\n" +
                    "Помилка:\n\n" + e.getMessage(), "",
                    JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }
        return connection;
    }

    public static Connection getConnection() {
       
            if (connection != null) { //пробуємо перевіряти до 20 секунд
                return connection;
            }
            return instance.createConnection();
       
       
    }
}
