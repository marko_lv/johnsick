package johnsick.model.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author JohnSick
 */
public class PropertyReader {

    private static PropertyReader instance = new PropertyReader();
    public static Map<String, String> properties = null;

    private void ReadProperties() {
        properties = new HashMap<>();
        Properties prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream("my.properties");

            prop.load(new InputStreamReader(input, Charset.forName("UTF-8")));
            //            prop.load(PropertyReader.class.getResourceAsStream("/p.properties"));

            for (Object keyObject : prop.keySet()) {
                String key = keyObject.toString();
                properties.put(key, prop.getProperty(key));
            }

        } catch (IOException ex) {
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
        }

    }

    /**
     * Витягує з конфіг файлу значення за його ключем.
     *
     * @param key ключ все що до знака =
     * @return значення, все після знака =
     */
    public static String GetValue(String key) {
        if (properties == null) {
            instance.ReadProperties();
        }
        String result = "";
        if (!key.isEmpty()) {
            result = properties.get(key);
        }
        return result;
    }

    public static List<String> GetAllKeys() {
        List<String> keys = new ArrayList<>();
        if (properties == null) {
            instance.ReadProperties();
        }
        Iterator it = properties.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            keys.add(pair.getKey().toString());
            
        }
        return keys;
    }

}
