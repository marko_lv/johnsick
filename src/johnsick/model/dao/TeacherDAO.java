/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package johnsick.model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import johnsick.model.Teacher;
import static johnsick.model.db.ConnectionFactory.connection;
import johnsick.model.db.DbUtil;

/**
 *
 * @author JohnSick
 */
public class TeacherDAO {

    private PreparedStatement ps;

    public TeacherDAO() {
    }

    private Teacher getTeacherFromResultset(ResultSet rs) {
        Teacher teacher = new Teacher();
        try {
            teacher.setId(rs.getLong("id"));
            teacher.setFirstName(rs.getString("firstname"));
            teacher.setMiddleName(rs.getString("middlename"));
            teacher.setLastName(rs.getString("lastname"));
            teacher.setAddress(rs.getString("address"));
            teacher.setPhone(rs.getString("phone"));
        } catch (SQLException ex) {
            Logger.getLogger(TeacherDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return teacher;
    }

    /**
     * Gets all Teachers from database
     *
     * @return
     */
    public List<Teacher> GetAll() {
        ResultSet rs = null;
        List<Teacher> result = new ArrayList<>();
        try {
            //  connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement("SELECT * FROM teachers ORDER BY lastname,firstname,middlename");
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(getTeacherFromResultset(rs));
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
       
        return result;
    }

    /**
     * Gets all students by filtered text
     *
     * @param filterText
     * @return
     */
    public List<Teacher> GetAll(String filterText) {
        return GetAll()
                .stream()
                .filter(st -> st.toString().toLowerCase().contains(filterText.toLowerCase()))
                .collect(Collectors.toList());
    }
}
