/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package johnsick.model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import johnsick.model.Fakultatyv;
import johnsick.model.db.ConnectionFactory;
import static johnsick.model.db.ConnectionFactory.connection;
import johnsick.model.db.DbUtil;

/**
 *
 * @author JohnSick
 */
public class FakultatyvDAO {

    private PreparedStatement ps;

    /*
    (Integer id, 
            String name, 
            Double lectionHoursCount, 
            Double practicHoursCount, 
            Double laborHoursCount, 
            Date startDate, 
            Date endDate, 
            List<Student> subscribedStudents)
     */
    private Fakultatyv getFakultatyvFromResultset(ResultSet rs) {
        Fakultatyv fak = null;
        try {
            fak = new Fakultatyv(
                    rs.getLong("id"),
                    rs.getString("name"),
                    rs.getDouble("laborhours"),
                    rs.getDouble("practichours"),
                    rs.getDouble("lectionhours"),
                    rs.getDate("startdate"),
                    rs.getDate("stopdate"),
                    new StudentDAO().getAllByFakultatyv(rs.getLong("id")),
                    rs.getLong("teacherid")
            );
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fak;
    }

    /**
     * Gets Fakultatyv from database
     *
     * @param fakultatyvId
     * @return
     */
    public Fakultatyv get(Long fakultatyvId) {

        ResultSet rs = null;
        Fakultatyv result = null;
        try {
            connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement("SELECT * FROM fakultatyvs WHERE id=?");
            ps.setLong(1, fakultatyvId);
            rs = ps.executeQuery();
            if (rs.next()) {
                result = getFakultatyvFromResultset(rs);
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        return result;
    }

    /**
     * Gets all Fakultatyv from database
     *
     * @return
     */
    public List<Fakultatyv> getAll() {

        ResultSet rs = null;
        List<Fakultatyv> result = new ArrayList<>();
        try {
            connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement("SELECT * FROM fakultatyvs ORDER BY `name`");
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(getFakultatyvFromResultset(rs));
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        return result;
    }

    /**
     * Gets all FakultativsIDs for teacher
     *
     * @param teacherId
     * @return
     */
    public String getAllTeacherFakultativsIds(Long teacherId) {

        ResultSet rs = null;
        String result = "";
        try {
            connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement("SELECT fakultatyvid FROM fakultatyvsforteachers WHERE teacherid=? ");
            ps.setLong(1, teacherId);
            rs = ps.executeQuery();
            while (rs.next()) {
                result = result + rs.getInt(1) + ",";
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        if (!result.isEmpty()) {
            result = "(" + result.substring(0, result.length() - 1) + ")";
        }
        return result;
    }

    /**
     * Gets all Fakultatyv from database by TeacherID
     *
     * @param teacherId
     * @return
     */
    public List<Fakultatyv> getAllByTeacher(Long teacherId) {

        ResultSet rs = null;
        List<Fakultatyv> result = new ArrayList<>();
        
        try {
            connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement("SELECT * FROM fakultatyvs WHERE teacherid =? ORDER BY `name`");
            ps.setLong(1, teacherId);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(getFakultatyvFromResultset(rs));
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        List<Fakultatyv> result1 = new ArrayList<>();
        Fakultatyv fak = new Fakultatyv(-1L, "Виберіть факультатив", null, null, null, null, null, null, teacherId);
        result1.add(fak);
        result1.addAll(result);
        return result1;
    }

    /**
     * Gets all Fakultatyv from database filtered by name
     *
     * @param filterText
     * @return
     */
    public List<Fakultatyv> getAll(String filterText) {
        return FakultatyvDAO.this.getAll()
                .stream()
                .filter(st -> st.toString().toLowerCase().contains(filterText.toLowerCase()))
                .collect(Collectors.toList());
    }

    /**
     * Insert or Update Fakultatyv into table
     *
     * @param fakultatyv
     * @return
     */
    public Fakultatyv saveFakultatyv(Fakultatyv fakultatyv) {
        try {
            if (fakultatyv.getId() == null || fakultatyv.getId() == -1) {
                ps = connection.prepareStatement("INSERT INTO fakultatyvs "
                        + "(`name`, laborhours, practichours, lectionhours, startdate, stopdate) "
                        + "VALUES(?, ?, ?, ?, ?, ?, ?)");
                int i = 1;
                ps.setString(i++, fakultatyv.getName());
                ps.setDouble(i++, fakultatyv.getLaborHoursCount());
                ps.setDouble(i++, fakultatyv.getPracticHoursCount());
                ps.setDouble(i++, fakultatyv.getLectionHoursCount());
                ps.setDate(i++, fakultatyv.getStartDate());
                ps.setDate(i++, fakultatyv.getEndDate());
                ps.setLong(i++, fakultatyv.getTeacherid());
                ps.executeUpdate();
                ps = connection.prepareStatement("SELECT last_insert_rowid();");
                ResultSet rs = ps.executeQuery();
                if (rs.next()) {
                    fakultatyv.setId(rs.getLong(1));
                }

            } else {
                ps = connection.prepareStatement("UPDATE fakultatyvs SET `name`=?, " 
                        + " laborhours=?, practichours=?, lectionhours=?, " 
                        + "startdate=?, stopdate=?, teacherid=?   WHERE id=?");
                int i = 1;
                ps.setString(i++, fakultatyv.getName());
                ps.setDouble(i++, fakultatyv.getLaborHoursCount());
                ps.setDouble(i++, fakultatyv.getPracticHoursCount());
                ps.setDouble(i++, fakultatyv.getLectionHoursCount());
                ps.setDate(i++, fakultatyv.getStartDate());
                ps.setDate(i++, fakultatyv.getEndDate());
                ps.setLong(i++, fakultatyv.getTeacherid());
                ps.setLong(i++, fakultatyv.getId());
                ps.executeUpdate();
            }
            DbUtil.close(ps);
        } catch (SQLException ex) {
            Logger.getLogger(Fakultatyv.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fakultatyv;
    }
}
