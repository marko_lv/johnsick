/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package johnsick.model.dao;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import johnsick.model.Fakultatyv;
import johnsick.model.Visit;
import johnsick.model.db.ConnectionFactory;
import static johnsick.model.db.ConnectionFactory.connection;
import johnsick.model.db.DbUtil;

/**
 *
 * @author dzoncick
 */
public class VisitDAO {

    private PreparedStatement ps;

    private Visit getVisitFromResultset(ResultSet rs) {
        Visit fak = null;
        try {
            fak = new Visit(
                    rs.getLong("fakultatyvid"),
                    rs.getLong("studentid"),
                    rs.getDate("visitdate"),
                    rs.getBoolean("visited"),
                    rs.getDouble("lectionmark"),
                    rs.getDouble("practicmark"),
                    rs.getDouble("labormark"),
                    rs.getDouble("lectionh"),
                    rs.getDouble("practich"),
                    rs.getDouble("laborh")
            );
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return fak;
    }

    public Visit getFirst(Date date) {
        ResultSet rs = null;
        Visit result = null;
        try {
            connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement("SELECT * FROM visits WHERE visitdate = ? ORDER BY visitdate LIMIT 1");
            ps.setDate(1, date);
            rs = ps.executeQuery();
            if (rs.next()) {
                result = getVisitFromResultset(rs);
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        return result;
    }
    
    public List<Visit> getAll() {
        ResultSet rs = null;
        List<Visit> result = new ArrayList<>();
        try {
            connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement("SELECT * FROM visits ORDER BY visitdate");
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(getVisitFromResultset(rs));
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        return result;
    }
    
     public List<Visit> getAll(Date date) {
        ResultSet rs = null;
        List<Visit> result = new ArrayList<>();
        try {
            connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement("SELECT * FROM visits WHERE visitdate = ?");
            ps.setDate(1, date);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(getVisitFromResultset(rs));
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        return result;
    }
    
    public List<Date> getAllVisitIds(Long fakultatyvId) {
        ResultSet rs = null;
        List<Date> result = new ArrayList<>();
        try {
            connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement("SELECT visitdate FROM visits WHERE fakultatyvid=? GROUP BY visitdate");
            ps.setLong(1, fakultatyvId);
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(rs.getDate("visitdate"));
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        return result;
    }
    
    
    public void save(Visit visit) {
        try {
            ps = connection.prepareStatement("INSERT INTO visits "
                    + "(fakultatyvid, studentid, visitdate, visited, lectionmark, practicmark, labormark, lectionh, practich, laborh) "
                    + "VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
            int i = 1;
            ps.setLong(i++, visit.getFakulatyvId());
            ps.setLong(i++, visit.getStudentId());
            ps.setDate(i++, visit.getVisitDate());
            ps.setBoolean(i++, visit.getVisited());
            ps.setDouble(i++, visit.getLectionMark());
            ps.setDouble(i++, visit.getPracticMark());
            ps.setDouble(i++, visit.getLaborMark());
            ps.setDouble(i++, visit.getLectionHoursCount());
            ps.setDouble(i++, visit.getPracticHoursCount());
            ps.setDouble(i++, visit.getLaborHoursCount());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(VisitDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
