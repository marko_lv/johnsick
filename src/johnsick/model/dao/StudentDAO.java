/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package johnsick.model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import johnsick.model.Student;
import static johnsick.model.db.ConnectionFactory.connection;
import johnsick.model.db.DbUtil;

/**
 *
 * @author JohnSick
 */
public class StudentDAO {

    private PreparedStatement ps;

    public StudentDAO() {
    }

    private Student getStudentFromResultset(ResultSet rs) {
        Student student = new Student();
        try {
            student.setId(rs.getLong("id"));
            student.setFirstName(rs.getString("firstname"));
            student.setMiddleName(rs.getString("middlename"));
            student.setLastName(rs.getString("lastname"));
            student.setAddress(rs.getString("address"));
            student.setPhone(rs.getString("phone"));
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return student;
    }

    /**
     * Gets all students from database
     *
     * @return
     */
    public List<Student> getAll() {
        ResultSet rs = null;
        List<Student> result = new ArrayList<>();
        try {
            //  connection = ConnectionFactory.getConnection();
            ps = connection.prepareStatement("SELECT * FROM students ORDER BY lastname,firstname,middlename");
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(getStudentFromResultset(rs));
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        return result;
    }

    /**
     * Gets all StudentsIDs for fakultativ
     *
     * @param fakultativId
     * @return
     */
    public String getAllFakultatyvStudentsIds(Long fakultativId) {

        ResultSet rs = null;
        String result = "";
        try {
            ps = connection.prepareStatement("SELECT studentid FROM fakultatyvsforstudents WHERE fakultatyvid=? ");
            ps.setLong(1, fakultativId);
            rs = ps.executeQuery();
            while (rs.next()) {
                result = result + rs.getInt(1) + ",";
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        if (!result.isEmpty()) {
            result = "(" + result.substring(0, result.length() - 1) + ")";
        }
        return result;
    }

    /**
     * Gets all students for particular Fakulatyv from database
     *
     * @param fakultatyvId
     * @return
     */
    public List<Student> getAllByFakultatyv(Long fakultatyvId) {
        ResultSet rs = null;
        List<Student> result = new ArrayList<>();
        String inStr = getAllFakultatyvStudentsIds(fakultatyvId);
        if (inStr.isEmpty()) {
            return result;
        }
        try {
            ps = connection.prepareStatement("SELECT * FROM students WHERE id IN "
                    + inStr
                    + "ORDER BY lastname,firstname,middlename");
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(getStudentFromResultset(rs));
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        return result;
    }

    /**
     * Gets all available students by filtered text
     *
     * @param filterText
     * @return
     */
    public List<Student> getAll(String filterText) {
        return StudentDAO.this.getAll()
                .stream()
                .filter(st -> st.toString().toLowerCase().contains(filterText.toLowerCase()))
                .collect(Collectors.toList());
    }

    /**
     * Gets all students from database
     *
     * @param selectedList
     * @return
     */
    public List<Student> getAllNotInList(List<Student> selectedList) {
        String str = "";
        for (Student st : selectedList) {
            if (st == null) {
                continue;
            }
            str = str + st.getId().toString() + ",";
        }
        if (!str.isEmpty()) {
            str = " WHERE id NOT IN (" + str.substring(0, str.length() - 1) + ") ";
        }
        ResultSet rs = null;
        List<Student> result = new ArrayList<>();
        try {
            ps = connection.prepareStatement("SELECT * FROM students " + str + " ORDER BY lastname,firstname,middlename");
            rs = ps.executeQuery();
            while (rs.next()) {
                result.add(getStudentFromResultset(rs));
            }
        } catch (SQLException ex) {
        } finally {
            DbUtil.close(rs);
            DbUtil.close(ps);
        }
        return result;
    }

    public void saveStudentsForFakuatyv(List<Long> studentsIds, Long fakultatyvId) {
        try {
            ps = connection.prepareStatement("DELETE FROM fakultatyvsforstudents WHERE fakultatyvId=?");
            ps.setLong(1, fakultatyvId);
            ps.executeUpdate();
            
            for (Long studentId : studentsIds) {
                ps = connection.prepareStatement("INSERT INTO fakultatyvsforstudents "
                        + "(studentid, fakultatyvid) "
                        + "VALUES(?, ?)");
                ps.setLong(1, studentId);
                ps.setLong(2, fakultatyvId);
                ps.executeUpdate();
            }
        } catch (SQLException ex) {
            Logger.getLogger(StudentDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
