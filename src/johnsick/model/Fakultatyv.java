/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package johnsick.model;

import java.sql.Date;
import java.util.List;

/**
 *
 * @author JohnSick
 */
public class Fakultatyv {
    private Long id;
    private String name;
    private Double lectionHoursCount;
    private Double practicHoursCount;
    private Double laborHoursCount;
    private Date startDate;
    private Date endDate;
    private List<Student> subscribedStudents;
    private Long teacherid;

    
    public Fakultatyv(Long id, 
            String name, 
            Double lectionHoursCount, 
            Double practicHoursCount, 
            Double laborHoursCount, 
            Date startDate, 
            Date endDate, 
            List<Student> subscribedStudents,
            Long teacherid) {
        this.id = id;
        this.name = name;
        this.lectionHoursCount = lectionHoursCount;
        this.practicHoursCount = practicHoursCount;
        this.laborHoursCount = laborHoursCount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.subscribedStudents = subscribedStudents;
        this.teacherid = teacherid;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLectionHoursCount() {
        return lectionHoursCount;
    }

    public void setLectionHoursCount(Double lectionHoursCount) {
        this.lectionHoursCount = lectionHoursCount;
    }

    public Double getPracticHoursCount() {
        return practicHoursCount;
    }

    public void setPracticHoursCount(Double practicHoursCount) {
        this.practicHoursCount = practicHoursCount;
    }

    public Double getLaborHoursCount() {
        return laborHoursCount;
    }

    public void setLaborHoursCount(Double laborHoursCount) {
        this.laborHoursCount = laborHoursCount;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Student> getSubscribedStudents() {
        return subscribedStudents;
    }

    public void setSubscribedStudents(List<Student> subscribedStudents) {
        this.subscribedStudents = subscribedStudents;
    }

    public Long getTeacherid() {
        return teacherid;
    }

    public void setTeacherid(Long teacherid) {
        this.teacherid = teacherid;
    }

    @Override
    public String toString() {
        return name;
    }
    
    
}
