/*
 * Don't ever think that this application was created by Swiatoslaw.
 * He is too stupid or lazy to create such code
 * He paid equivalent of 100USD which were donated for charity purposes.
 * Don't be like Swiatoslaw!
 * Make the code by yourself!
 */
package johnsick.model;

/**
 *
 * @author JohnSick
 */
public class Student extends Person {

    public String getFullName() {
        return super.getLastName() + " " + super.getFirstName() + " " + super.getMiddleName();
    }

    @Override
    public String toString() {
        return super.getLastName() + " " + super.getFirstName() + " " + super.getMiddleName();
    }

}
